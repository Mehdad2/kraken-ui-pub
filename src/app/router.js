import { Switch, Route, Router as BrowseRouter } from 'react-router-dom'
import  List  from '../features/list/list'
import ListDetail from '../features/list/listDetail'
import { createBrowserHistory } from 'history';

const history = createBrowserHistory({});

const Router = () => {
  return (
      <BrowseRouter history={history}>
        <Switch>
            <Route exact path="/" component={List} />
            <Route path="/list/:id" component={ListDetail} />
        </Switch>
      </BrowseRouter>
  )
}

export default Router
