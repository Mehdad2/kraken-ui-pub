import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { BASE, API_KEY, SESSION_ID, ACCOUNT_ID } from '../utils'

export const listApi = createApi({
    reducerPath: "listApi",// We only specify this because there are many services.
    baseQuery: fetchBaseQuery({ baseUrl: BASE }),
    tagTypes: ['List'],
    endpoints: (builder) => ({ 
        getAccountList: builder.query({
            query:() => `/account/${ACCOUNT_ID}/lists?api_key=${API_KEY}&session_id=${SESSION_ID}`,
            providesTags: ['List']
          }),
        addList: builder.mutation({
            query:(body) => ({
                url: `/list?api_key=${API_KEY}&session_id=${SESSION_ID}`,
                method:'POST',
                body:body
            }),
            invalidatesTags: ['List']
        }),
    })
})

export const { 
    useGetAccountListQuery,
    useAddListMutation,
 } = listApi

