import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { BASE, API_KEY, SESSION_ID } from '../utils'

export const MovieApi = createApi({
    reducerPath: "MovieApi",
    baseQuery: fetchBaseQuery({ baseUrl: BASE }),
    tagTypes: ['Movies'],
    endpoints: (builder) => ({
        getMoviesFromList: builder.query({
          query:(id) => `/list/${id}?api_key=${API_KEY}`,
          providesTags: ["Movies"]
        }),
        addMovie: builder.mutation({
            query:({list_id,...body}) => ({
                url: `/list/${list_id}/add_item?api_key=${API_KEY}&session_id=${SESSION_ID}`,
                method:'POST',
                body
            }),
            invalidatesTags: ["Movies"]
        }),
        searchMovie: builder.query({
            query:(search) => ({
                url: `/search/movie?api_key=${API_KEY}&page=1&query=${search}&page=1`
            })
        })
    })
})

export const { 
    useGetMoviesFromListQuery,
    useAddMovieMutation,
    useSearchMovieQuery
 } = MovieApi

