import { configureStore } from '@reduxjs/toolkit';
import { listApi }  from './services/list'
import { MovieApi } from './services/movie'


export const store = configureStore({
  reducer: {
    [listApi.reducerPath]: listApi.reducer,
    [MovieApi.reducerPath]: MovieApi.reducer
  },
    // adding the api middleware enables caching, invalidation, polling and other features of `rtk-query`
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(listApi.middleware, MovieApi.middleware),
  
});
