import React from 'react'
import './button.css'
import plus from './plus.svg'
const Button = (props) => {
    const  { handleClick, label }  = props
    return (
        <button className='btn' onClick={handleClick}>
           <img src={plus} alt="add-svg" width={50}  height={50}/>
        </button>
    ) 
}

export default Button