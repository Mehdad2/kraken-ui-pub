import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";
import Button from '../../components/button/button';
import './list.css'
import AddList from './modal/addList';
import { useGetAccountListQuery } from '../../app/services/list'

const List = () => {
    const history = useHistory()
    const [ display, setDisplay ] = useState(false)
    const { data:  accountList, isLoading, isSuccess, isError, error } = useGetAccountListQuery()
    
    const handleAddList = () => {
        setDisplay(!display)
    }
  
    return (
        <>
            {!isError && <Button {...{label: 'add', handleClick: handleAddList}} />}
            <div className={ isLoading ? "container-loader" : isError ? 'container-error' : 'container' }>
                {isLoading && '...Loading'}
                {isError && error.data.status_message}
                {isSuccess && accountList && accountList.results.length > 0 && accountList.results.map((l) =>
                <div className="container__list-card" key={l.id} onClick={() => 
                    history.push(`/list/${l.id}`)
                } >
                    <h2>{l.name}</h2>
                    <h3>({l.item_count})</h3>
                </div>
                )}
            </div>
            {display && <AddList {...{handleClose: handleAddList }} />}
        </>

    )

}

export default List