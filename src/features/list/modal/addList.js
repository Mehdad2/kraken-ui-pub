import React, { useState, useEffect } from 'react'
import close from '../../../components/button/cancel.svg'
import { useAddListMutation } from '../../../app/services/list'
import '../list.css'
const AddList = (props) => {
    const { handleClose  } = props
    const [ formVal, setFormVal ] = useState({name:"", description:"", language:""})
    const [ addList, { isLoading, isError, error, isSuccess } ] = useAddListMutation()
    useEffect(() => {
        if(isSuccess) {
            handleClose()
        }
    }, [isSuccess])

    const handleSubmit = async (e) =>  {
        e.preventDefault()
        try {
            const payload = await addList(formVal).unwrap()
            console.log(payload)
        } catch(err) {
            console.error(err)
        }
    }

    const handleChange = (e) => {
        setFormVal({...formVal,[e.target.name]: e.target.value})
    }

    return(
        <div className="modal-addList" >
            <div className="modal-addList__content">
                <h1>Add a New List
                <img className="modal-addList__img" src={close} alt="close-svg" onClick={handleClose} width={20}  height={20}/>
                </h1>
                {isError && <div className="modal-addList__error">{error.data.status_message}</div>}
                <form className="form" onSubmit={handleSubmit} onChange={handleChange}>
                    <div className="form-group1">
                        <div>
                            <label htmlFor="name"> Name </label>
                        </div>
                        <div>
                            <input type="text" name="name" defaultValue={formVal.name}/>
                        </div>
                    </div>
                    <div className="form-group2">
                        <div>
                            <label htmlFor="language"> Language </label>
                        </div>
                        <div>
                            <input type="text" name="language" defaultValue={formVal.language} />
                        </div>
                    </div>
                    <div className="form-group3">
                        <div>
                            <label htmlFor="description"> Description </label>
                        </div>
                        <div>
                            <textarea name="description" rows={10}  cols={10}  defaultValue={formVal.description} />
                        </div>
                    </div>
                    <div className="form-group4">
                        <input type="submit" value="Envoyer"  disabled={isLoading}/>
                    </div>
                </form>
            </div>
        </div>
    )

}

export default AddList