import React, { useState, useEffect } from 'react'
import close from '../../../components/button/cancel.svg'
import { useAddMovieMutation, useSearchMovieQuery } from '../../../app/services/movie'
import SearchList from '../searchList'
import '../list.css'

const AddMovie = (props) => {
    const { handleClose, list_id  } = props
    const [ formVal, setFormVal ] = useState({search:"", description:"", language:""})
    const [ skip, setSkip ] = useState(true)

    const { data: movie, isSuccess, isError, error } = useSearchMovieQuery(formVal.search,{
        skip: skip,
    })
    const [ addMovie, { 
          isError: isErrorAddMovie,
          error: errMovie, 
          isSuccess: isSuccessAddMovie } 
        ] = useAddMovieMutation()
   
    useEffect(() => {
        if(isSuccessAddMovie) {
            handleClose()
        }
    }, [isSuccessAddMovie])

    const handleAddMovie = async (id) =>  {
        await addMovie({list_id,"media_id": id})
    }

    const handleChange = async (e) => {
        setFormVal({...formVal,[e.target.name]: e.target.value})
        setSkip(false)
    }
    
    return(
        <div className="modal-addmovie" >
            <div className="modal-addmovie__content">
                <h1>Add a New Movie
                <img className="modal-addmovie__img" src={close} alt="close-svg" onClick={handleClose} width={20}  height={20}/>
                </h1>
                {isError && <div className="modal-addmovie__error">{error.data.status_message}</div>}
                {isErrorAddMovie && <div className="modal-addmovie__error">{errMovie.data.status_message }</div>}
                <form className="form" onChange={handleChange}>
                    <div className="form-search">
                        <div>
                            <label htmlFor="name"> Search Movie to add </label>
                        </div>
                        <div>
                            <input className='form-search__input' type="text" name="search" defaultValue={formVal.search}/>
                        </div>
                        {isSuccess && movie && movie.results.length > 0 && 
                            <SearchList {...{
                                movies: movie.results, 
                                handleAddMovie}}
                            />
                        }
                    </div>
                </form>
            </div>
        </div>
    )

}

export default AddMovie