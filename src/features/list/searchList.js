import React from 'react'
import './list.css'
const SearchList = (props) => {
    const  { movies, handleAddMovie }  = props

    return (
        <div className="movie-searchList">
            {movies.map((e) => 
                <div className={`movie-searchList__title ${e.id}`} key={e.id} onClick={() => handleAddMovie(e.id)}>
                    <h5>
                        <img 
                        src={`https://www.themoviedb.org/t/p/w90_and_h134_bestv2/${e.poster_path}`} 
                        alt="logo-movie"  
                        width={20} 
                        />
                        {e.original_title}
                    </h5>
                </div>
            )}
        </div>  
    )

}
export default SearchList



