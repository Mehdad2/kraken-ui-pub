import React, { useState } from 'react'
import { useParams } from "react-router-dom";
import { useGetMoviesFromListQuery } from '../../app/services/movie'
import  AddMovie  from './modal/addMovie'
import './list.css'

const ListDetail = () => {
    const { id } = useParams()
    const [ display, setDisplay ] = useState(false)
    const { data:  moviesList, isLoading, isSuccess, isError, error } = useGetMoviesFromListQuery(id)
    const handleAddMovie = () => {
        setDisplay(!display)

    }
    return(
        <div  className="movies">
            <div className={"movies__header"}>
            <div className={"movies__header--item1"}>
                {isSuccess && `created by ${moviesList.created_by}`}
            </div>
            <div className="movies__header--item2">
                {isSuccess && moviesList.item_count} elements
            </div>
            <div className="movies__header--item3">
                <button onClick={handleAddMovie}>Add Movie</button>
            </div>
            </div>
            <div className="movies__list">
                {isSuccess && moviesList.items.length > 0 && moviesList.items.map((m) => 
                    <React.Fragment key={m.id} >
                        <img 
                        src={`https://www.themoviedb.org/t/p/w370_and_h556_multi_faces/${m.poster_path}`} 
                        alt='poster' 
                        width="25%"
                     
                        />
                    </React.Fragment>
                )

                }
            </div>

            {display && <AddMovie {...{handleClose: handleAddMovie, list_id : id }} />}
        </div>
        
    )
}

export default ListDetail