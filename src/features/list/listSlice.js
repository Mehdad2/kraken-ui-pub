import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    loading: false,
    movies: [],
    lists: []

}

const listMoviesSlice = createSlice({
    name: "listSlice",
    initialState,
    reducers: {
        getLists: (state,action) => {
            state.lists = action.payload
        },
        getMovies: (state,action) => {
            state.movies = action.payload
        },
        setLoader: (state, action) => {
            state.loading = action.payload 
        }
    }


})

export const  { 
    getLists,
    setLoader,
    getMovies } = listMoviesSlice.actions

export default listMoviesSlice.reducer